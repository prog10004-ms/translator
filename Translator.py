"""
Implements the translation between one langauge and another

@Author: Prof. Magdin Stoica
@Course: Programming Priciples, PROG10004
"""
#add the capability of this module to translate strings. How?
#module "capabilities" are implemented through functions.
#define a function to translate a string input into binary language
def translate(inputText):
    """Translates the input text into binary langauge and return the translation to the caller"""

    #remember the translation 
    binTranslation = ''

    #repeat translating characters as many times as the length of the input
    for index in range(len(inputText)):
        #translate one character
        #convert the first character of the input to its number value
        numValue = ord(inputText[index])

        #translate the  number value into binary
        binValue = bin(numValue)[2:]

         #add the character traslation to the result
        binTranslation = binTranslation + binValue + ' ' #can also be written as binTranslation += hBinValue + ' '
    
    #return the translation to the caller
    return binTranslation
"""
Implements the interactivity with the user to ask for data to be translated and display
the result of the translation

@Author: Prof. Magdin Stoica
@Course: Programming Priciples, PROG10004
"""
import Translator

#ask the user for an text input
userInput = input('Please enter the text to translate to binary: ')

#translate the input using the Translator module
trans = Translator.translate(userInput)

#print out the translation
print(f'The translation of "{userInput}" is {trans}')